import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class WriteList {
	private Scanner in;
	private File inputFile;
	
	public List<String> createList(int recordLength, String fileNameIn){
		System.out.println("final record length in "+ recordLength);
		List<String> arr = new ArrayList<>();
		//String fileNameIn = "temp.txt";
		 try {
		        File inputFile = new File(fileNameIn);
		        Scanner in = new Scanner(inputFile);

		        this.inputFile = inputFile;
		        this.in = in;
		    } catch (IOException exception) {
		        System.out.println("Could not open the file");
		    }
		
		System.out.println("test1.4");
		int rl=1;
		int fl=0;
		if(recordLength==874){	
			while (in.hasNextLine()) {
			   	String line = in.nextLine();
	        	//System.out.println("Length of line: "+line.length());
	        	String noColumns = "";
	        	noColumns=removeExtraColumns874(line);
	        
	        	//System.out.println(noColumns);
	        
	        	rl =noColumns.length();
	        	//System.out.println("Length of line post columns: "+rl);
	        	arr.add(noColumns);
	        	fl++;
	        	//System.out.println("file length: "+fl + " array size: "+arr.size());
	        
			}
		}if(recordLength==865){	
			while (in.hasNextLine()) {
			   	String line = in.nextLine();
	        	//System.out.println("Length of line: "+line.length());
	        	String noColumns = "";
	        	noColumns=removeExtraColumns865(line);
	        
	        	//System.out.println(noColumns);
	        
	        	rl =noColumns.length();
	        	//System.out.println("Length of line post columns: "+rl);
	        	arr.add(noColumns);
	        	fl++;
	        	//System.out.println("file length: "+fl + " array size: "+arr.size());
	        }
		}
		else{ System.out.println("record length doesnt match");}
		
		
		return arr;
	}
	
	public String removeExtraColumns874(String s){
		int lp =s.length();
		String e = ""; 
		String data = "";
		String alphaCode ="";
		data = s.substring(0,864);
		alphaCode = s.substring(904,lp);		
		//System.out.println("front line "+data +	"\nalpha " +alphaCode);		
		e=e.concat(data+alphaCode);
		//System.out.println("e: "+e);
		//System.out.println(e.length());
		
		return e;
	}
	public String removeExtraColumns865(String s){
		int lp =s.length();
		String e = ""; 
		String data = "";
		String alphaCode ="";
		data = s.substring(0,855);
		alphaCode = s.substring(904,lp);		
		System.out.println("data length:"+data.length()+":front line "+data + 
				"\nalpha length:"+alphaCode.length()+":alpha " +alphaCode);		
		e=e.concat(data+alphaCode);
		//System.out.println("e: "+e);
		//System.out.println(e.length());
		
		return e;
	}

	
}
