
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class main
{
	
  public static void main(String[] args){	
	 
	  
	  VerifyFile vf = new VerifyFile();
	  GetFileInfo GFI = new GetFileInfo();
	  WriteList WL = new WriteList();	
	  WriteFile WF = new WriteFile();
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {	
    	  GUI G = new GUI();
    	  
    	  
    	  List<String> arr = new ArrayList<>();
    	  
    	  boolean realFile = false;
    	  
    	  File filePath = new File(System.getProperty("user.dir"));

          File[] listingAllFiles = filePath.listFiles();

          ArrayList<File> allFiles = GFI.iterateOverFiles(listingAllFiles);

    	  for(int j=0;j<allFiles.size();j++){
		    		  int orl=0;  
		    		  String plantCode, outputFileName = "";
		        	  String fileNameIn ="";
		          fileNameIn = allFiles.get(j).toString();
		    	/*File[] files = new File(System.getProperty("user.dir")).listFiles();
		     	GetFileInfo.showFiles(files);*/
		     	/********  verify input file exists **************/
		     	  realFile = vf.verify(fileNameIn);
		     	  System.out.println(realFile);
			     	if(realFile==true){
			     	/********  get record length **************/
			     		orl=GFI.rowLength(fileNameIn);
			     	// int RL = GRL.rowLength(fileNameIn);
			     	/********  get record amount **************/	
			     	int ora=GFI.getNumberOfLines(fileNameIn);
			     	System.out.println("ora: "+ora);
			     	
			     	/******** determine plant code  **************/
			     	plantCode = GFI.getPlantCode(fileNameIn);
			     	
			     	/******** determine output file name  **************/
			     	outputFileName=WF.determineFileName(plantCode);
			     	
			     	/******** determine record length of output file  **************/
			     	boolean longRecord = false;
			     	int rec = 0;
			     	longRecord = GFI.shortOrLong(plantCode);
			     	
			     	/********  Set the final file length after removing the extra columns **************/
			     	if(longRecord==true){
			     		rec=874;
			     	}else{
			     		rec=865;
			     	}
			     	
			     	/********  Create the new list from the original file **************/	
			     	arr=WL.createList(rec,fileNameIn);  	
			     	
			     	/******** determine if output file name exists **************/
			     	boolean verifyOutputfileDoesntExist=false; System.out.println("at output filename 1: "+outputFileName);
			     	verifyOutputfileDoesntExist=vf.verify(outputFileName);
			     	/******** if output file name exists add time stamp to the end**************/
			     	if(verifyOutputfileDoesntExist==true){outputFileName=WF.ifOutputFileExists(outputFileName);}
			     	
			     	/********  write the new file **************/
			     	WF.useBufferedFileWriter(arr, outputFileName);
			     	System.out.println(orl);
			       	System.out.println("adios");     	
			     	
			       	/********  verify the output file **************/
			       	boolean correctOF=false;
			       	System.out.println("|"+outputFileName+"|");
			       	correctOF=vf.verify(outputFileName);
			       	
			       	/********  verify the correct record length **************/
			       	boolean correctRL=false;
			       	int frl = GFI.rowLength(outputFileName);
			       	System.out.println("File Record length: "+frl+" rec: "+rec);
			       	if(rec==frl) correctRL=true;
			       	
			       	
			       	/********  verify the correct record amount **************/
			       	boolean correctRA=false;
			       	int fra=GFI.getNumberOfLines(outputFileName);
			       	if(ora == fra)correctRA =true;
			       	
			       	/********  alert success or failure **************/
			       	if (correctOF==true && correctRL==true && correctRA==true){
			       		G.success("Success " + outputFileName + " has been created.");
			       	}else{G.success("FAILURE " + outputFileName + " has NOT been created OR has NOT passed one or more of the file checks below.\nCheck 1: Outputfile created: " + correctOF 
			       			+"\nCheck 2: correct record length: "+correctRL
			       			+"\nCheck 3: correct amount of records: "+ correctRA);
			       		}  
			       	//System.exit(0);
			     	}
    	  }
    	  }
    });
  }
}
