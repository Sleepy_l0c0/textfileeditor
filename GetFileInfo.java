import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;




public class GetFileInfo {

private File inputFile;
private Scanner in;
	
	public void GetLength(String string) throws FileNotFoundException {
	    try {
	        File inputFile = new File(string);
	        Scanner in = new Scanner(inputFile);

	        this.inputFile = inputFile;
	        this.in = in;
	    } catch (IOException exception) {
	        System.out.println("Could not open the file 1");
	    }
	}
	public int getNumberOfLines(String fileNameIn) {
		int ra=0;
		try {
	        File inputFile = new File(fileNameIn);
	        Scanner in = new Scanner(inputFile);
	        this.inputFile = inputFile;
	        this.in = in;
		
		while (in.hasNextLine()) {
	        String line = in.nextLine();
	        ra =ra+1;
	        //System.out.println(rl);
		}}catch (IOException exception) {
	        System.out.println("Could not open the file 2");
	    }
		//System.out.println(ra);		
		return ra;
	}
	public int rowLength(String fileNameIn){
		int rl=1;
		try {
	        File inputFile = new File(fileNameIn);
	        Scanner in = new Scanner(inputFile);
	        this.inputFile = inputFile;
	        this.in = in;
		
		while (in.hasNextLine()) {
	        String line = in.nextLine();
	        rl =line.length();
	        //System.out.println("@rl: "+ rl);
		}}catch (IOException exception) {
	        System.out.println("Could not open the file 3");
	    }
				
		return rl;
	}
	
public String getPlantCode(String fileNameIn){
	String rl,pc="";
	try {
        File inputFile = new File(fileNameIn);
        Scanner in = new Scanner(inputFile);
        this.inputFile = inputFile;
        this.in = in;
	
	while (in.hasNextLine()) {
        String line = in.nextLine();
        rl =line.substring(904, line.length());
        //System.out.println(rl.trim());
        pc=rl.trim();
	}}catch (IOException exception) {
        System.out.println("Could not open the file 4");
    }
			
	return pc;
}
public boolean shortOrLong(String f){
	boolean a=false;
	
	if(f.equalsIgnoreCase("adfc") || f.equalsIgnoreCase("PDMT") || f.equalsIgnoreCase("LSMP WEST") || f.equalsIgnoreCase("LSMP EAST")) {a = true;}
	
	return a;
}
public static void showFiles(File[] files) {
    for (File file : files) {
        if (file.isDirectory()) {
            System.out.println("Directory: " + file.getName());
            showFiles(file.listFiles()); // Calls same method again.
        } else {
            System.out.println("File: " + file.getName());
        }
    }
}

public static ArrayList<File> iterateOverFiles(File[] files) {
	ArrayList<File> al = new ArrayList<File>();
    File fileLocation = null;

    for (File file : files) {

        if (file.isDirectory()) {
        	System.out.println("do nothing");
            //iterateOverFiles(file.listFiles());// Calls same method again.

        } else {

            fileLocation = findFileswithTxtExtension(file);
            if(fileLocation != null) {
                System.out.println(fileLocation);
                al.add(fileLocation);
            }


        }
    }

    return al;
}
public static File findFileswithTxtExtension(File file) {

    if(file.getName().toLowerCase().endsWith("txt")) {
        return file;
    }

    return null;
}
}
