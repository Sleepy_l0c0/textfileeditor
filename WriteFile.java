import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class WriteFile {
	
	public void useBufferedFileWriter(List<String> content,
            String filePath) {
		//System.out.println("test1.5 " + filePath);
        File file = new File(filePath);
        Writer fileWriter = null;
        BufferedWriter bufferedWriter = null;
 
        try {
        	//System.out.println("test1.6");
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
            //System.out.println("test1.65");
            //System.out.println(content.size());
            // Write the lines one by one
            for (String line : content) {
            	//System.out.println("test1.7");
            	//System.out.println("line: "+line);
                line += System.getProperty("line.separator");
                bufferedWriter.write(line);
 
                // alternatively add bufferedWriter.newLine() to change line
            }
 
        } catch (IOException e) {
        	//System.out.println("test1.8");
            System.err.println("Error writing the file : ");
            e.printStackTrace();
        } finally {
        	//System.out.println("test1.9");
            if (bufferedWriter != null && fileWriter != null) {
                try {
                	//System.out.println("test1.10");
                    bufferedWriter.close();
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	public String determineFileName(String f){
		String finalFileName="";
		String time = getTimeStamp();
		f=f.trim();
		if(f.equalsIgnoreCase("adfc")|| f.equalsIgnoreCase("PDMT")) {finalFileName="hmpspaytr";}
		else if(f.equalsIgnoreCase("LSMP WEST") || f.equalsIgnoreCase("LSMP EAST")){finalFileName="lsmppaytr";}
		else if(f.equalsIgnoreCase("BORDEN")){finalFileName="bmadpaytr";}
		else if(f.equalsIgnoreCase("COBBLE")){finalFileName="cobbpaytr";}
		else if(f.equalsIgnoreCase("DEAN")||f.equalsIgnoreCase("prod_coop")){finalFileName="ddirpaytr";}
		else if(f.equalsIgnoreCase("lanco")){finalFileName="lancpaytr";}
		else if(f.equalsIgnoreCase("lofco")){finalFileName="lofcpaytr";}
		else finalFileName ="output"+time;
		return finalFileName;
	}
	public String ifOutputFileExists(String dynamicFileNameIn){
		String dfniTimeStamp="";
		String time = getTimeStamp();
		dfniTimeStamp=dfniTimeStamp.concat(dynamicFileNameIn+time);		
		return dfniTimeStamp;
	}
	public String getTimeStamp(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String time = sdf.format(timestamp);
		
		return time;
	}
}
