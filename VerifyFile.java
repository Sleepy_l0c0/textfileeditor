import java.io.File;

public class VerifyFile {
	
	public boolean verify(String fileNameIn){
		boolean exists = false;
		String currentDir = System.getProperty("user.dir");
		System.out.println(currentDir);
		File myFile = new File(fileNameIn); 
		System.out.println("testing for this fileName: "+myFile+":");
		if (myFile.exists()) {
			System.out.println(myFile.getName() + " exists");
			System.out.println("The file is " + myFile.length() + " bytes long");
			if (myFile.canRead())
				{ System.out.println(" ok to read"); }
			else
				{ System.out.println(" not ok to read"); }
			if (myFile.canWrite())
				{	System.out.println(" ok to write");}
			else {System.out.println(" not ok to write");}
			/*System.out.println("path: " +myFile.getAbsolutePath());
			System.out.println("File Name: "+ myFile.getName());
			System.out.println("File Size: "+ myFile.length() + " bytes");*/
			exists =true;
		} else
			System.out.println("File does not exist");
	
		return exists;
	}
}
